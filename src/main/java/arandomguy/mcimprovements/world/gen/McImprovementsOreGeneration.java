package arandomguy.mcimprovements.world.gen;

import arandomguy.mcimprovements.lists.BlockList;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.registries.ForgeRegistries;

public class McImprovementsOreGeneration {

    private static void setupOreGeneration() {
        for(Biome biome : ForgeRegistries.BIOMES) {
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES,
                    Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                            BlockList.copper_ore.getDefaultState(), 4))
                            .withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(6, 10, 0, 30))));
        }
    }

    public static void generate() {
        setupOreGeneration();
    }
}

//Count is how common it is to spawn, so if you set it really high, then there will be a lot of your ore in the world.
//
//BottomOffset is how far from the bottom it can spawn. So if you set it to 15 then the ore won’t spawn below level 15.
//
//Just set topOffset to 0
//
//Maximum is similar to BottomOffset, however, it is for the highest point. So if you set it to 25, then the ore cannot spawn above level 25 in the world