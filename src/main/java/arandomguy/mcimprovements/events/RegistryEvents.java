package arandomguy.mcimprovements.events;

import arandomguy.mcimprovements.McImprovements;
import arandomguy.mcimprovements.blocks.CopperOre;
import arandomguy.mcimprovements.itemGroups.*;
import arandomguy.mcimprovements.lists.BlockList;
import arandomguy.mcimprovements.lists.FoodList;
import arandomguy.mcimprovements.lists.ItemList;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public final class RegistryEvents {

    public static final Logger LOGGER = McImprovements.LOGGER;
    public static final String MOD_ID = McImprovements.MOD_ID;
    public static final ItemGroup BLOCKS_TAB = McImprovementsBlocksItemGroup.MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB;
    public static final ItemGroup ARMOUR_TAB = McImprovementsArmourItemGroup.MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB;
    public static final ItemGroup FOODS_TAB = McImprovementsFoodsItemGroup.MC_IMPROVEMENTS_FOOD_CREATIVE_TAB;
    public static final ItemGroup ITEM_TAB = McImprovementsItemGroup.MC_IMPROVEMENTS_ITEM_CREATIVE_TAB;
    public static final ItemGroup TOOLS_TAB = McImprovementsToolsItemGroup.MC_IMPROVEMENTS_TOOL_CREATIVE_TAB;

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(
                ItemList.copper_ingot = new Item(new Item.Properties().group(ITEM_TAB)).setRegistryName(location("copper_ingot")),

                ItemList.berry_cluster = new Item(new Item.Properties().group(FOODS_TAB).food(FoodList.berry_cluster)).setRegistryName(location("berry_cluster")),

                ItemList.copper_ore = new BlockItem(BlockList.copper_ore, new Item.Properties().group(BLOCKS_TAB)).setRegistryName(BlockList.copper_ore.getRegistryName())
        );
        LOGGER.info("Items registered.");
    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(
                BlockList.copper_ore = new CopperOre().setRegistryName(location("copper_ore"))
        );
        LOGGER.info("Blocks registered.");
    }

    public static ResourceLocation location(String name) {
        return new ResourceLocation(MOD_ID, name);
    }
}
