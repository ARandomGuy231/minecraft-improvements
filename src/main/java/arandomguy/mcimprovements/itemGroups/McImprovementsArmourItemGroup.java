package arandomguy.mcimprovements.itemGroups;

import arandomguy.mcimprovements.lists.ItemList;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsArmourItemGroup extends ItemGroup {

    public static final ItemGroup MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB = new McImprovementsArmourItemGroup("MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB");

        public McImprovementsArmourItemGroup(String name) {
            super(name);
        }

        @Override
        public ItemStack createIcon() {
            return new ItemStack(ItemList.copper_chestplate);
        }
}
