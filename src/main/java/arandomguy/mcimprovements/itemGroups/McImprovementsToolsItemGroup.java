package arandomguy.mcimprovements.itemGroups;

import arandomguy.mcimprovements.lists.ItemList;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsToolsItemGroup extends ItemGroup {

    public static final ItemGroup MC_IMPROVEMENTS_TOOL_CREATIVE_TAB = new McImprovementsToolsItemGroup("MC_IMPROVEMENTS_TOOL_CREATIVE_TAB");

    public McImprovementsToolsItemGroup(String name) {
        super(name);
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(ItemList.copper_sword);
    }
}
