package arandomguy.mcimprovements.itemGroups;

import arandomguy.mcimprovements.lists.ItemList;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsItemGroup extends ItemGroup {

    public static final ItemGroup MC_IMPROVEMENTS_ITEM_CREATIVE_TAB = new McImprovementsItemGroup("MC_IMPROVEMENTS_ITEM_CREATIVE_TAB");

    public McImprovementsItemGroup(String name) {
        super(name);
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(ItemList.copper_ingot);
    }
}
