package arandomguy.mcimprovements.itemGroups;

import arandomguy.mcimprovements.lists.ItemList;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsBlocksItemGroup extends ItemGroup {

    public static final ItemGroup MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB = new McImprovementsBlocksItemGroup("MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB");

    public McImprovementsBlocksItemGroup(String name) {
        super(name);
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(ItemList.copper_ore);
    }
}
