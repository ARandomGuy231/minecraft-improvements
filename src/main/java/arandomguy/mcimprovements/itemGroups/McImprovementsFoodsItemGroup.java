package arandomguy.mcimprovements.itemGroups;

import arandomguy.mcimprovements.lists.ItemList;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsFoodsItemGroup extends ItemGroup {

    public static final ItemGroup MC_IMPROVEMENTS_FOOD_CREATIVE_TAB = new McImprovementsFoodsItemGroup("MC_IMPROVEMENTS_FOOD_CREATIVE_TAB");

    public McImprovementsFoodsItemGroup(String name) {
        super(name);
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(ItemList.berry_cluster);
    }
}
