package arandomguy.mcimprovements.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class ColouredWood extends Block {
    public ColouredWood() {
        super(Properties.create(Material.WOOD)
                .sound(SoundType.WOOD)
                .harvestLevel(0)
                .hardnessAndResistance(2.0F)
        );

    }
}
