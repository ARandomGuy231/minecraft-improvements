package arandomguy.mcimprovements.blocks;

import net.minecraft.block.OreBlock;
import net.minecraft.util.math.MathHelper;

import java.util.Random;

public class CopperCustomOreBlock extends OreBlock {

    public CopperCustomOreBlock(Properties properties) {
        super(properties);
    }

    @Override
    protected int getExperience(Random random) {
        return MathHelper.nextInt(random, 0, 5);
    }
}
