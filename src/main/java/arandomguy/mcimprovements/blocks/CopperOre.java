package arandomguy.mcimprovements.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class CopperOre extends Block {

    public CopperOre() {
        super(Properties.create(Material.ROCK)
                .sound(SoundType.STONE)
                .harvestLevel(2)
                .hardnessAndResistance(2.0F)
        );

    }

}
