package arandomguy.mcimprovements;

import arandomguy.mcimprovements.lists.ItemList;
import arandomguy.mcimprovements.world.gen.McImprovementsOreGeneration;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(McImprovements.MOD_ID)
public class McImprovements {

    public static McImprovements instance;
    public static final String MOD_ID = "mcimprovements";
    public static final Logger LOGGER = LogManager.getLogger();

    public McImprovements() {
        instance = this;
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);
    }

    private void setup(final FMLCommonSetupEvent event) {
        McImprovementsOreGeneration.generate();
    }

    private void clientSetup(final FMLClientSetupEvent event) {

    }

    private void onServerStarting(final FMLServerStartingEvent event) {

    }
}
