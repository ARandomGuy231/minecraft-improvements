package arandomguy315.mcimprovements.creativetabs;

import arandomguy315.mcimprovements.init.McImprovementsBlocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsBlockCreativeTab extends ItemGroup {

	public McImprovementsBlockCreativeTab() {
		super("MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB");
	}

	@Override
	public ItemStack createIcon() {
		return new ItemStack(McImprovementsBlocks.copper_ore);
	}

}
