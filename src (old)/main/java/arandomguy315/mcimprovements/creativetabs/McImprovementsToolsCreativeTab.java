package arandomguy315.mcimprovements.creativetabs;

import arandomguy315.mcimprovements.init.McImprovementsItems;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsToolsCreativeTab extends ItemGroup {

	public McImprovementsToolsCreativeTab() {
		super("MC_IMPROVEMENTS_TOOL_CREATIVE_TAB");
	}

	@Override
	public ItemStack createIcon() {
		return new ItemStack(McImprovementsItems.redstone_sword);
	}

}
