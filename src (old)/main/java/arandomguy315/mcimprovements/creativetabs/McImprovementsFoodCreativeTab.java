package arandomguy315.mcimprovements.creativetabs;

import arandomguy315.mcimprovements.init.McImprovementsItems;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsFoodCreativeTab extends ItemGroup {

    public McImprovementsFoodCreativeTab() {
        super("MC_IMPROVEMENTS_FOOD_CREATIVE_TAB");
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(McImprovementsItems.berry_cluster);
    }
}
