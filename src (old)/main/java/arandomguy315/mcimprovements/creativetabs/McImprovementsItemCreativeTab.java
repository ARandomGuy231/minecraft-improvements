package arandomguy315.mcimprovements.creativetabs;

import arandomguy315.mcimprovements.init.McImprovementsItems;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class McImprovementsItemCreativeTab extends ItemGroup {

	public McImprovementsItemCreativeTab() {
		super("MC_IMPROVEMENTS_ITEM_CREATIVE_TAB");
	}

	@Override
	public ItemStack createIcon() {
		return new ItemStack(McImprovementsItems.copper_ingot);
	}

}
