package arandomguy315.mcimprovements;

import arandomguy315.mcimprovements.proxy.ClientProxy;
import arandomguy315.mcimprovements.proxy.IProxy;
import arandomguy315.mcimprovements.proxy.ServerProxy;
import arandomguy315.mcimprovements.world.gen.features.McImprovementsOreGeneration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.Logger;

@Mod("mcimprovements")
public class MinecraftImprovements {

	public static IProxy proxy = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);
	private static MinecraftImprovements instance;
	static final String MODID = "mcimprovements";
	static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MODID);

	public MinecraftImprovements() {
		instance = this;
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::ClientRegistries);
		MinecraftForge.EVENT_BUS.register(this);
	}

	private void setup(final FMLCommonSetupEvent event) {
		McImprovementsOreGeneration.registerOreGeneration();
		LOGGER.info("Setup method registered.");
	}

	private void ClientRegistries(final FMLClientSetupEvent event) {
		LOGGER.info("ClientRegistries method registered.");
	}
}
