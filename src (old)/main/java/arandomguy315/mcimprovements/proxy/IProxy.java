package arandomguy315.mcimprovements.proxy;

import net.minecraft.world.World;

public interface IProxy {

    World getClientWorld();
}
