package arandomguy315.mcimprovements.world.gen.features;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;
import com.mojang.datafixers.Dynamic;
import com.mojang.datafixers.types.DynamicOps;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.pattern.BlockMatcher;
import net.minecraft.world.gen.feature.IFeatureConfig;

public class McImprovementsOreFeatureConfig implements IFeatureConfig {

    public final McImprovementsFillerBlockType target;
    public final int size;
    public final BlockState state;

    public McImprovementsOreFeatureConfig(McImprovementsFillerBlockType target, BlockState state, int size)
    {
        this.size = size;
        this.state = state;
        this.target = target;
    }

    public <T> Dynamic<T> serialize(DynamicOps<T> ops)
    {
        return new Dynamic<>(ops, ops.createMap(ImmutableMap.of(ops.createString("size"), ops.createInt(this.size), ops.createString("target"), ops.createString(this.target.func_214737_a()), ops.createString("state"), BlockState.serialize(ops, this.state).getValue())));
    }

    public static McImprovementsOreFeatureConfig deserialize(Dynamic<?> p_214641_0_)
    {
        int i = p_214641_0_.get("size").asInt(0);
        McImprovementsFillerBlockType fillerBlockType = McImprovementsFillerBlockType.func_214736_a(p_214641_0_.get("target").asString(""));
        BlockState blockstate = p_214641_0_.get("state").map(BlockState::deserialize).orElse(Blocks.AIR.getDefaultState());
        return new McImprovementsOreFeatureConfig(fillerBlockType, blockstate, i);
    }

    public static enum McImprovementsFillerBlockType
    {
        NATURAL_STONE("natural_stone", (p_214739_0_) ->
        {
            if (p_214739_0_ == null)
            {
                return false;
            }
            else
            {
                Block block = p_214739_0_.getBlock();
                return block == Blocks.STONE || block == Blocks.GRANITE || block == Blocks.DIORITE || block == Blocks.ANDESITE;
            }
        }
        ),
        NETHERRACK("netherrack", new BlockMatcher(Blocks.NETHERRACK)),
        ENDSTONE("endstone", new BlockMatcher(Blocks.END_STONE)),
        SANDSTONE("sandstone", new BlockMatcher(Blocks.SANDSTONE));

        private static final Map<String, McImprovementsFillerBlockType> fillerTypes = Arrays.stream(values()).collect(Collectors.toMap(McImprovementsFillerBlockType::func_214737_a, (p_214740_0_) ->
        {
            return p_214740_0_;
        }));
        private final String registryName;
        private final Predicate<BlockState> blockstate;

        private McImprovementsFillerBlockType(String name, Predicate<BlockState> block)
        {
            this.registryName = name;
            this.blockstate = block;
        }

        public String func_214737_a()
        {
            return this.registryName;
        }

        public static McImprovementsFillerBlockType func_214736_a(String p_214736_0_)
        {
            return fillerTypes.get(p_214736_0_);
        }

        public Predicate<BlockState> func_214738_b() {
            return this.blockstate;
        }
    }
}
