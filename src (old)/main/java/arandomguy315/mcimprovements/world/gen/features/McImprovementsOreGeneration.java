package arandomguy315.mcimprovements.world.gen.features;

import arandomguy315.mcimprovements.init.McImprovementsBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.registries.ForgeRegistries;

public class McImprovementsOreGeneration {

    private static CountRangeConfig copper_ore_gen;
    private static CountRangeConfig opal_ore_gen;

    public static void registerOreGeneration() {
        for(Biome biome : ForgeRegistries.BIOMES) {
            addOreGen(copper_ore_gen, 6, 10, 10, 30, biome, McImprovementsOreFeatureConfig.McImprovementsFillerBlockType.NATURAL_STONE, McImprovementsBlocks.copper_ore.getDefaultState(), 4);
            addOreGen(opal_ore_gen, 14, 40, 40, 90, biome, McImprovementsOreFeatureConfig.McImprovementsFillerBlockType.SANDSTONE, McImprovementsBlocks.opal_ore.getDefaultState(), 5);
        }


    }

    private static void addOreGen(CountRangeConfig placement, int chance, int minHeight, int maxHeightBase, int maxHeight, Biome biome, McImprovementsOreFeatureConfig.McImprovementsFillerBlockType type, BlockState block, int veinSize) {
        placement = new CountRangeConfig(chance, minHeight, maxHeightBase, maxHeight);
        biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Biome.createDecoratedFeature(McImprovementsFeatures.MOD_ORE, new McImprovementsOreFeatureConfig(type, block, 8), Placement.COUNT_RANGE, placement));
    }
}
