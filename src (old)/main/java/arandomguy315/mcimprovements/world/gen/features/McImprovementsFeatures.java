package arandomguy315.mcimprovements.world.gen.features;

        import arandomguy315.mcimprovements.MinecraftImprovementsRegistries;
        import net.minecraft.util.ResourceLocation;
        import net.minecraft.world.gen.feature.Feature;
        import net.minecraftforge.eventbus.api.SubscribeEvent;
        import net.minecraftforge.registries.GameData;
        import net.minecraftforge.registries.IForgeRegistry;
        import net.minecraftforge.registries.IForgeRegistryEntry;
        import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(MinecraftImprovementsRegistries.MODID)
public class McImprovementsFeatures {

    public static final Feature<McImprovementsOreFeatureConfig> MOD_ORE = new McImprovementsOreFeature(McImprovementsOreFeatureConfig::deserialize);

    @SubscribeEvent
    public static void registerFeatures(IForgeRegistry<Feature<?>> event)
    {
        generic(event).add("mod_ore", MOD_ORE);
    }

    public static <T extends IForgeRegistryEntry<T>> Generic<T> generic(IForgeRegistry<T> registry)
    {
        return new Generic<>(registry);
    }

    public static class Generic<T extends IForgeRegistryEntry<T>>
    {
        private final IForgeRegistry<T> registry;

        private Generic(IForgeRegistry<T> registry)
        {
            this.registry = registry;
        }

        public Generic<T> add(String name, T entry)
        {
            ResourceLocation registryName = GameData.checkPrefix(name, false);
            entry.setRegistryName(registryName);
            this.registry.register(entry);
            return this;
        }
    }
}
