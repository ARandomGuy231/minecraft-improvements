package arandomguy315.mcimprovements.init;

import net.minecraft.block.Block;

public class McImprovementsBlocks {

	public static Block copper_ore;
	public static Block copper_block;
	public static Block opal_ore;
	public static Block opal_block;
	public static Block wood_plank_white;
	public static Block wood_plank_red;
	public static Block wood_plank_orange;
	public static Block wood_plank_pink;
	public static Block wood_plank_yellow;
	public static Block wood_plank_lime;
	public static Block wood_plank_green;
	public static Block wood_plank_light_blue;
	public static Block wood_plank_cyan;
	public static Block wood_plank_blue;
	public static Block wood_plank_magenta;
	public static Block wood_plank_purple;
	public static Block wood_plank_brown;
	public static Block wood_plank_gray;
	public static Block wood_plank_light_gray;
	public static Block wood_plank_black;
}
