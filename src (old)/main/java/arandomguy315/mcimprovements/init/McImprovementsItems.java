package arandomguy315.mcimprovements.init;

import net.minecraft.item.Item;

public class McImprovementsItems {
	
	public static Item copper_ingot;
	public static Item opal;
	
	public static Item copper_ore;
	public static Item copper_block;
	public static Item opal_ore;
	public static Item opal_block;
	public static Item wood_plank_white;
	public static Item wood_plank_red;
	public static Item wood_plank_orange;
	public static Item wood_plank_pink;
	public static Item wood_plank_yellow;
	public static Item wood_plank_lime;
	public static Item wood_plank_green;
	public static Item wood_plank_light_blue;
	public static Item wood_plank_cyan;
	public static Item wood_plank_blue;
	public static Item wood_plank_magenta;
	public static Item wood_plank_purple;
	public static Item wood_plank_brown;
	public static Item wood_plank_gray;
	public static Item wood_plank_light_gray;
	public static Item wood_plank_black;

	public static Item copper_axe;
	public static Item copper_hoe;
	public static Item copper_pickaxe;
	public static Item copper_shovel;
	public static Item copper_sword;
	
	public static Item redstone_axe;
	public static Item redstone_hoe;
	public static Item redstone_pickaxe;
	public static Item redstone_shovel;
	public static Item redstone_sword;

	public static Item opal_axe;
	public static Item opal_hoe;
	public static Item opal_pickaxe;
	public static Item opal_shovel;
	public static Item opal_sword;
	
	public static Item copper_helmet;
	public static Item copper_chestplate;
	public static Item copper_leggings;
	public static Item copper_boots;
	
	public static Item redstone_helmet;
	public static Item redstone_chestplate;
	public static Item redstone_leggings;
	public static Item redstone_boots;

	public static Item opal_helmet;
	public static Item opal_chestplate;
	public static Item opal_leggings;
	public static Item opal_boots;

	public static Item berry_cluster;

}
