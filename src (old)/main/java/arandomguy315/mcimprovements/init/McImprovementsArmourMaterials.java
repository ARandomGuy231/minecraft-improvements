package arandomguy315.mcimprovements.init;

import arandomguy315.mcimprovements.MinecraftImprovementsRegistries;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public enum McImprovementsArmourMaterials implements IArmorMaterial {
	
	copperArmourMaterial("copperarmourmaterial", 400, new int[]{2, 5, 7, 2}, 25, McImprovementsItems.copper_ingot, "entity.armor_stand.place", 0.5F),
	redstoneArmourMaterial("redstonearmourmaterial", 600, new int[]{4, 7, 9, 4}, 30, Items.REDSTONE, "entity.armor_stand.place", 1.0F),
	opalArmourMaterial("opalarmourmaterial", 1000, new int[]{6, 9, 11, 6}, 30, McImprovementsItems.opal, "entity.armor_stand.place", 2.0F);
	
	private final int[] max_damage_array = new int[]{13, 15, 16, 11};
	private String name, equipSound;
	private int durability, enchantability;
	private Item repairItem;
	private int[] damageReductionAmounts;
	private float toughness;
	
	private McImprovementsArmourMaterials(String name, int durability, int[] damageReductionAmounts, int enchantability, Item repairItem, String equipSound, float toughness) {
		this.name = name;
		this.equipSound = equipSound;
		this.durability = durability;
		this.enchantability = enchantability;
		this.repairItem = repairItem;
		this.damageReductionAmounts = damageReductionAmounts;
		this.toughness = toughness;
	}

	@Override
	public int getDamageReductionAmount(EquipmentSlotType slot) {
		return this.damageReductionAmounts[slot.getIndex()];
	}

	@Override
	public int getDurability(EquipmentSlotType slot) {
		return max_damage_array[slot.getIndex()] * this.durability;
	}

	@Override
	public int getEnchantability() {
		return this.enchantability;
	}

	@Override
	public String getName() {
		return MinecraftImprovementsRegistries.MODID + ":" + this.name;
	}

	@Override
	public Ingredient getRepairMaterial() {
		return Ingredient.fromItems(this.repairItem);
	}

	@Override
	public SoundEvent getSoundEvent() {
		return new SoundEvent(new ResourceLocation(equipSound));
	}

	@Override
	public float getToughness() {
		return this.toughness;
	}
}
