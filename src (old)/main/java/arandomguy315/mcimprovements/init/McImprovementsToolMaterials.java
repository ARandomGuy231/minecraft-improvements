package arandomguy315.mcimprovements.init;

import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;

public enum McImprovementsToolMaterials implements IItemTier {
	copperToolMaterial(5.5F, 8.5F, 750, 2, 15, McImprovementsItems.copper_ingot),
	redstoneToolMaterial(7.0F, 9.5F, 1000, 3, 20, Items.REDSTONE),
	opalToolMaterial(8.0F, 10.0F, 1400, 4, 30, McImprovementsItems.opal);
	
	private float attackDamage, efficiency;
	private int durability, harvestLevel, enchantability;
	private Item repairMaterial;
	
	McImprovementsToolMaterials(float attackDamage, float efficiency, int durability, int harvestLevel, int enchantability, Item repairMeterial) {
		this.attackDamage = attackDamage;
		this.efficiency = efficiency;
		this.durability = durability;
		this.harvestLevel = harvestLevel;
		this.enchantability = enchantability;
		this.repairMaterial = repairMeterial;
	}

	@Override
	public float getAttackDamage() {
		return this.attackDamage;
	}

	@Override
	public float getEfficiency() {
		return this.efficiency;
	}

	@Override
	public int getEnchantability() {
		return this.enchantability;
	}

	@Override
	public int getHarvestLevel() {
		return this.harvestLevel;
	}

	@Override
	public int getMaxUses() {
		return this.durability;
	}

	@Override
	public Ingredient getRepairMaterial() {
		return Ingredient.fromItems(this.repairMaterial);
	}
}
