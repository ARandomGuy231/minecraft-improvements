package arandomguy315.mcimprovements;

import arandomguy315.mcimprovements.blocks.*;
import arandomguy315.mcimprovements.creativetabs.*;
import arandomguy315.mcimprovements.init.*;
import net.minecraft.block.Block;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class MinecraftImprovementsRegistries {

    public static final ItemGroup MC_IMPROVEMENTS_ITEM_CREATIVE_TAB = new McImprovementsItemCreativeTab();
    public static final ItemGroup MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB = new McImprovementsArmourCreativeTab();
    public static final ItemGroup MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB = new McImprovementsBlockCreativeTab();
    public static final ItemGroup MC_IMPROVEMENTS_TOOL_CREATIVE_TAB = new McImprovementsToolsCreativeTab();
    public static final ItemGroup MC_IMPROVEMENTS_FOOD_CREATIVE_TAB = new McImprovementsFoodCreativeTab();
    private static final Logger LOGGER = MinecraftImprovements.LOGGER;
    public static final String MODID = MinecraftImprovements.MODID;


    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll
                (
                        McImprovementsItems.copper_ingot = new Item(new Item.Properties().group(MC_IMPROVEMENTS_ITEM_CREATIVE_TAB)).setRegistryName(location("copper_ingot")),
                        McImprovementsItems.opal = new Item(new Item.Properties().group(MC_IMPROVEMENTS_ITEM_CREATIVE_TAB)).setRegistryName(location("opal")),
                        McImprovementsItems.berry_cluster = new Item(new Item.Properties().food(McImprovementsFood.berry_cluster).group(MC_IMPROVEMENTS_FOOD_CREATIVE_TAB)).setRegistryName(location("berry_cluster")),

                        McImprovementsItems.copper_axe = new AxeItem(McImprovementsToolMaterials.copperToolMaterial, -1.0F, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("copper_axe")),
                        McImprovementsItems.copper_hoe = new HoeItem(McImprovementsToolMaterials.copperToolMaterial, 6.0F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("copper_hoe")),
                        McImprovementsItems.copper_pickaxe = new PickaxeItem(McImprovementsToolMaterials.copperToolMaterial, -2, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("copper_pickaxe")),
                        McImprovementsItems.copper_shovel = new ShovelItem(McImprovementsToolMaterials.copperToolMaterial, -3.0F, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("copper_shovel")),
                        McImprovementsItems.copper_sword = new SwordItem(McImprovementsToolMaterials.copperToolMaterial, 0, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("copper_sword")),

                        McImprovementsItems.redstone_axe = new AxeItem(McImprovementsToolMaterials.redstoneToolMaterial, -1.0F, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("redstone_axe")),
                        McImprovementsItems.redstone_hoe = new HoeItem(McImprovementsToolMaterials.redstoneToolMaterial, 6.0F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("redstone_hoe")),
                        McImprovementsItems.redstone_pickaxe = new PickaxeItem(McImprovementsToolMaterials.redstoneToolMaterial, -2, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("redstone_pickaxe")),
                        McImprovementsItems.redstone_shovel = new ShovelItem(McImprovementsToolMaterials.redstoneToolMaterial, -3.0F, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("redstone_shovel")),
                        McImprovementsItems.redstone_sword = new SwordItem(McImprovementsToolMaterials.redstoneToolMaterial, 0, -2.4F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("redstone_sword")),

                        McImprovementsItems.opal_axe = new AxeItem(McImprovementsToolMaterials.opalToolMaterial, -1.0F, -1.6F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("opal_axe")),
                        McImprovementsItems.opal_hoe = new HoeItem(McImprovementsToolMaterials.opalToolMaterial, 6.0F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("opal_hoe")),
                        McImprovementsItems.opal_pickaxe = new PickaxeItem(McImprovementsToolMaterials.opalToolMaterial, -2, -1.6F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("opal_pickaxe")),
                        McImprovementsItems.opal_shovel = new ShovelItem(McImprovementsToolMaterials.opalToolMaterial, -3.0F, -1.6F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("opal_shovel")),
                        McImprovementsItems.opal_sword = new SwordItem(McImprovementsToolMaterials.opalToolMaterial, 0, -1.6F, new Item.Properties().group(MC_IMPROVEMENTS_TOOL_CREATIVE_TAB)).setRegistryName(location("opal_sword")),

                        McImprovementsItems.copper_helmet = new ArmorItem(McImprovementsArmourMaterials.copperArmourMaterial, EquipmentSlotType.HEAD, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("copper_helmet")),
                        McImprovementsItems.copper_chestplate = new ArmorItem(McImprovementsArmourMaterials.copperArmourMaterial, EquipmentSlotType.CHEST, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("copper_chestplate")),
                        McImprovementsItems.copper_leggings = new ArmorItem(McImprovementsArmourMaterials.copperArmourMaterial, EquipmentSlotType.LEGS, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("copper_leggings")),
                        McImprovementsItems.copper_boots = new ArmorItem(McImprovementsArmourMaterials.copperArmourMaterial, EquipmentSlotType.FEET, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("copper_boots")),

                        McImprovementsItems.redstone_helmet = new ArmorItem(McImprovementsArmourMaterials.redstoneArmourMaterial, EquipmentSlotType.HEAD, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("redstone_helmet")),
                        McImprovementsItems.redstone_chestplate = new ArmorItem(McImprovementsArmourMaterials.redstoneArmourMaterial, EquipmentSlotType.CHEST, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("redstone_chestplate")),
                        McImprovementsItems.redstone_leggings = new ArmorItem(McImprovementsArmourMaterials.redstoneArmourMaterial, EquipmentSlotType.LEGS, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("redstone_leggings")),
                        McImprovementsItems.redstone_boots = new ArmorItem(McImprovementsArmourMaterials.redstoneArmourMaterial, EquipmentSlotType.FEET, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("redstone_boots")),

                        McImprovementsItems.opal_helmet = new ArmorItem(McImprovementsArmourMaterials.opalArmourMaterial, EquipmentSlotType.HEAD, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("opal_helmet")),
                        McImprovementsItems.opal_chestplate = new ArmorItem(McImprovementsArmourMaterials.opalArmourMaterial, EquipmentSlotType.CHEST, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("opal_chestplate")),
                        McImprovementsItems.opal_leggings = new ArmorItem(McImprovementsArmourMaterials.opalArmourMaterial, EquipmentSlotType.LEGS, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("opal_leggings")),
                        McImprovementsItems.opal_boots = new ArmorItem(McImprovementsArmourMaterials.opalArmourMaterial, EquipmentSlotType.FEET, new Item.Properties().group(MC_IMPROVEMENTS_ARMOUR_CREATIVE_TAB)).setRegistryName(location("opal_boots")),

                        McImprovementsItems.copper_ore = new BlockItem(McImprovementsBlocks.copper_ore, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.copper_ore.getRegistryName()),
                        McImprovementsItems.copper_block = new BlockItem(McImprovementsBlocks.copper_block, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.copper_block.getRegistryName()),
                        McImprovementsItems.opal_ore = new BlockItem(McImprovementsBlocks.opal_ore, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.opal_ore.getRegistryName()),
                        McImprovementsItems.opal_block = new BlockItem(McImprovementsBlocks.opal_block, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.opal_block.getRegistryName()),
                        McImprovementsItems.wood_plank_white = new BlockItem(McImprovementsBlocks.wood_plank_white, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_white.getRegistryName()),
                        McImprovementsItems.wood_plank_red = new BlockItem(McImprovementsBlocks.wood_plank_red, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_red.getRegistryName()),
                        McImprovementsItems.wood_plank_orange = new BlockItem(McImprovementsBlocks.wood_plank_orange, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_orange.getRegistryName()),
                        McImprovementsItems.wood_plank_pink = new BlockItem(McImprovementsBlocks.wood_plank_pink, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_pink.getRegistryName()),
                        McImprovementsItems.wood_plank_yellow = new BlockItem(McImprovementsBlocks.wood_plank_yellow, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_yellow.getRegistryName()),
                        McImprovementsItems.wood_plank_lime = new BlockItem(McImprovementsBlocks.wood_plank_lime, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_lime.getRegistryName()),
                        McImprovementsItems.wood_plank_green = new BlockItem(McImprovementsBlocks.wood_plank_green, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_green.getRegistryName()),
                        McImprovementsItems.wood_plank_light_blue = new BlockItem(McImprovementsBlocks.wood_plank_light_blue, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_light_blue.getRegistryName()),
                        McImprovementsItems.wood_plank_cyan = new BlockItem(McImprovementsBlocks.wood_plank_cyan, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_cyan.getRegistryName()),
                        McImprovementsItems.wood_plank_blue = new BlockItem(McImprovementsBlocks.wood_plank_blue, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_blue.getRegistryName()),
                        McImprovementsItems.wood_plank_magenta = new BlockItem(McImprovementsBlocks.wood_plank_magenta, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_magenta.getRegistryName()),
                        McImprovementsItems.wood_plank_purple = new BlockItem(McImprovementsBlocks.wood_plank_purple, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_purple.getRegistryName()),
                        McImprovementsItems.wood_plank_brown = new BlockItem(McImprovementsBlocks.wood_plank_brown, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_brown.getRegistryName()),
                        McImprovementsItems.wood_plank_gray = new BlockItem(McImprovementsBlocks.wood_plank_gray, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_gray.getRegistryName()),
                        McImprovementsItems.wood_plank_light_gray = new BlockItem(McImprovementsBlocks.wood_plank_light_gray, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_light_gray.getRegistryName()),
                        McImprovementsItems.wood_plank_black = new BlockItem(McImprovementsBlocks.wood_plank_black, new Item.Properties().group(MC_IMPROVEMENTS_BLOCK_CREATIVE_TAB)).setRegistryName(McImprovementsBlocks.wood_plank_black.getRegistryName())
                );
        LOGGER.info("Items registered.");
    }

    @SubscribeEvent
    public static void registerBlocks(final RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll
                (
                        McImprovementsBlocks.copper_ore = new CopperOre().setRegistryName(location("copper_ore")),
                        McImprovementsBlocks.copper_block = new CopperBlock().setRegistryName(location("copper_block")),
                        McImprovementsBlocks.opal_ore = new OpalOre().setRegistryName(location("opal_ore")),
                        McImprovementsBlocks.opal_block = new OpalBlock().setRegistryName(location("opal_block")),
                        McImprovementsBlocks.wood_plank_white = new ColouredWood().setRegistryName(location("wood_plank_white")),
                        McImprovementsBlocks.wood_plank_red = new ColouredWood().setRegistryName(location("wood_plank_red")),
                        McImprovementsBlocks.wood_plank_orange = new ColouredWood().setRegistryName(location("wood_plank_orange")),
                        McImprovementsBlocks.wood_plank_pink = new ColouredWood().setRegistryName(location("wood_plank_pink")),
                        McImprovementsBlocks.wood_plank_yellow = new ColouredWood().setRegistryName(location("wood_plank_yellow")),
                        McImprovementsBlocks.wood_plank_lime = new ColouredWood().setRegistryName(location("wood_plank_lime")),
                        McImprovementsBlocks.wood_plank_green = new ColouredWood().setRegistryName(location("wood_plank_green")),
                        McImprovementsBlocks.wood_plank_light_blue = new ColouredWood().setRegistryName(location("wood_plank_light_blue")),
                        McImprovementsBlocks.wood_plank_cyan = new ColouredWood().setRegistryName(location("wood_plank_cyan")),
                        McImprovementsBlocks.wood_plank_blue = new ColouredWood().setRegistryName(location("wood_plank_blue")),
                        McImprovementsBlocks.wood_plank_magenta = new ColouredWood().setRegistryName(location("wood_plank_magenta")),
                        McImprovementsBlocks.wood_plank_purple = new ColouredWood().setRegistryName(location("wood_plank_purple")),
                        McImprovementsBlocks.wood_plank_brown = new ColouredWood().setRegistryName(location("wood_plank_brown")),
                        McImprovementsBlocks.wood_plank_gray = new ColouredWood().setRegistryName(location("wood_plank_gray")),
                        McImprovementsBlocks.wood_plank_light_gray = new ColouredWood().setRegistryName(location("wood_plank_light_gray")),
                        McImprovementsBlocks.wood_plank_black = new ColouredWood().setRegistryName(location("wood_plank_black"))
                );

        LOGGER.info("Blocks registered.");
    }

    public static ResourceLocation location(String name) {
        return new ResourceLocation(MODID, name);
    }
}
